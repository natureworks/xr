# Citizen Particiipation Links

* Deliberative process. International research into deliberative democracy over 40 years  
https://www.oecd.org/gov/innovative-citizen-participation-and-new-democratic-institutions-339306da-en.htm
* Clear simple comprehensive guide to successful citizens assemblies based on experience  
https://citizensassemblies.org/
* UK Climate assembly. BBC documentary in UK climate assembly, shows the impact on participants, the reality of the process  
https://involve.org.uk/resources/blog/project-updates/watch-climate-assembly-uk-documentary-bbc-iplayer
* Impact on assembly members. Study shows the lasting change to assembly members (panel or jurors). It shows the potential for creating a policy literate and empowered public  
https://involve.org.uk/resources/blog/project-updates/climate-assembly-members-think-and-act-differently-climate-two-years
* Wales assemblies. Wales citizen assembly 1919 on democratic processes  
  * https://participedia.net/case/6087
  * https://devolution20.wales/citizens-assembly
  * https://involve.org.uk/resources/blog/project-update/wales-first-citizens-assembly-reports-back
  * https://www.iwa.wales/agenda/2020/07/why-we-need-a-welsh-citizens-assembly-on-the-climate-crisis/
  * https://nation.cymru/opinion/citizens-assemblies-will-be-key-to-reshaping-wales-after-covid-19/
  * https://academiwales.gov.wales/events/view/a6ff99b9-8971-4ba8-b8d1-febc56a0d7d4
* Wales Peoples Assembly on food, farming, land use, interview - (3rd section down)  
https://www.grwp.wales/photos-videos
* Presentation by Grant Peisley, GwyrddNi on climate assemblies in Gwynedd. Passcode: =kfp183K
JK8znrdIe5dVivsc8tTK576ovfW_LkPwAGB9.hC-4iHt3CDjOKH2k?startTime=1673870824000
* Organisations providing services 
  * The sortition foundation – provides sortition and communication to panel members up to start of assembly.
  * The Involve foundation – runs assemblies, facilitation etc
  * UK study of councils response to citizens assemblies/juries  
    https://constitution-unit.com/2022/03/25/local-citizens-assemblies-in-the-uk-a-second-report-card/
  * Tool for sortition  
    https://panelot.org/


