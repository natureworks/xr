## XR Aberteifi socials

https://bit.ly/xrbuffer

* Accounts
    * Buffer https://publish.buffer.com
    * Facebook https://facebook.com/groups/extinctionrebellioncardigan
    * Twitter https://twitter.com/XRAberteifi
    * Mastodon https://mas.to/@XRAberteifi
* Sources
    * XR buffer https://codeberg.org/natureworks/xr/src/branch/main/buffer/buffer.md
    * Telegram group 
    * Minutes 
    * Newsletter 
    * Social channels
    * XRUK live action content channel Telegram (ask for link to join)
    * News #ClimateEmergency #XR #ExtinctionRebellion #JustStopOil
* Regular stuff
    * Kinora - Extinction Rebellion social, 1st Friday of month 6-8pm, Kinora, Cardigan SA43 1DR https://res.cloudinary.com/dd3ymdmgw/image/upload/v1685564680/kinora-cardigan.jpg
    * Street stall - Generally weekly, days & times vary, outside Guildhall, Cardigan https://res.cloudinary.com/dd3ymdmgw/image/upload/v1686131889/guildhall-cardigan.jpg
    * Meeting Monday 5.15-6pm online 
    * Climate Resilience Show - Thursday 5-6pm, Cardigan Internet Radio, https://cardiganinternetradio.wales/ClimateResilienceShow/ https://res.cloudinary.com/dd3ymdmgw/image/upload/v1685564680/climate-resilience-show_jp2nfn.jpg
    * Newsletter
    * Website https://xraberteifi.uk -- NOT YET!
    * Sign up for our newsletter and national Extinction Rebellion news https://actionnetwork.org/forms/yes-xr-cymru/ 

## Contact

```
cardiganxr@protonmail.com 
07805 214 391 
```