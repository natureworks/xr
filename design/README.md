# XR design

* Colours in PDF/SVG/PNG file
* Fonts in this directory
  * FUCXED Latin for headlines
  * [Crimson Pro](https://fonts.google.com/specimen/Crimson+Pro?query=crimson+pro) Regular, Italic, Bold, Bold Italic for body copy